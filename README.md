# rx_minesweeper

A [Minesweeper](https://en.wikipedia.org/wiki/Minesweeper_(video_game)) clone in Flutter, runnable on iOS, Android, Web

## Demo

https://truongsinh.gitlab.io/flutter_rx_minesweeper/#/

On web, due to pre-alpha status of Flutter Web, there are caveats:
- significant performance degrade
- no custom fonts
- unaligned emojis 💣 🚩 🤔, there are even differences in rendering between Chrome and Firefox
- flagging using longPress instead of rightClick (convinient on mobile, but unnatural on desktop)

Other caveats (general):
- Game params can only be changed during compile time
- First revealed square are not guanranteed to be safe
