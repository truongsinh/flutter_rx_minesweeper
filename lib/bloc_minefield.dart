import 'dart:math';

import 'package:rxdart/rxdart.dart';
import 'package:rxdart/subjects.dart';

import 'interface_minecell.dart';
import 'interface_minefield.dart';

import 'bloc_minecell.dart';

// @todo not guaranteed to be cryptographically secured
// @todo any chance 2 remaining bomb, 1 remaining cell?
bool shouldThisCellRandomlyIsBomb(
  int totalRemainingBomb,
  int totalRemainingCellInclusive,
  Random randomGen,
) {
  assert(totalRemainingBomb >= 0);
  assert(totalRemainingCellInclusive >= 1);
  if (totalRemainingBomb == 0) {
    return false;
  }
  if (totalRemainingCellInclusive == 1) {
    return true;
  }
  final randomValue = randomGen.nextDouble();
  return randomValue < totalRemainingBomb / totalRemainingCellInclusive;
}

class MineField implements IMineField {
  final Point<int> dimension;
  final Random randomGen;
  final List<List<IBlocMineCell>> fieldRows;
  // ignore: close_sinks
  final BehaviorSubject<Null> firstCellsAreOpened = BehaviorSubject<Null>();
  // ignore: close_sinks
  final BehaviorSubject<Null> allCellsAreOpened = BehaviorSubject<Null>();
  MineField(this.dimension, totalBomb, {int randomSeed})
      : randomGen = Random(randomSeed),
        fieldRows = List<List<IBlocMineCell>>(dimension.y) {
    assert(dimension.y > 0);
    assert(dimension.x > 0);
    assert(totalBomb >= 0);

    int remainingCell = dimension.x * dimension.y;
    int remaininBomb = totalBomb;

    for (var thisRow = 0; thisRow < dimension.y; thisRow++) {
      final fieldCellsInARow = List<IBlocMineCell>(dimension.x);
      fieldRows[thisRow] = fieldCellsInARow;
      for (var thisColumn = 0; thisColumn < dimension.x; thisColumn++) {
        // @todo we can have even more decoupled
        final mineCell = BlocMineCell();
        if (shouldThisCellRandomlyIsBomb(
            remaininBomb, remainingCell, randomGen)) {
          mineCell.isBombSubject.add(true);
          remaininBomb--;
        }
        remainingCell--;
        fieldCellsInARow[thisColumn] = mineCell;

        _connectThisCellToItsNeighbor(thisRow, thisColumn, mineCell);
      }
    }

    final totonalNonBombCells = this.dimension.x * this.dimension.y - totalBomb;
    final flattenedBlocCellPresentation = fieldRows
        .expand((list) => list)
        .map((blocCell) => blocCell.cellPresentation);

    final mergedCellPresnetaion =
        Observable.merge(flattenedBlocCellPresentation).asBroadcastStream();

    mergedCellPresnetaion
        .where((thisPresnetation) =>
            thisPresnetation.index <= MineCellPresentation.bomb.index)
        .bufferCount(this.dimension.x * this.dimension.y - totalBomb)
        .where((l) => l.length == totonalNonBombCells)
        .where((allPreviousPresentation) =>
            !allPreviousPresentation.any((p) => p == MineCellPresentation.bomb))
        .map((_) => null)
        .pipe(allCellsAreOpened);

    mergedCellPresnetaion
        .where((thisPresnetation) =>
            thisPresnetation.index < MineCellPresentation.bomb.index)
        .where((p) => p != MineCellPresentation.bomb)
        .take(1)
        .map((_) => null)
        .pipe(firstCellsAreOpened);
  }

  /*
  void reset(int totalBomb) {
    int remainingCell = dimension.x * dimension.y;
    int remaininBomb = totalBomb;
    for (var thisRow = 0; thisRow < dimension.y; thisRow++) {
      for (var thisColumn = 0; thisColumn < dimension.x; thisColumn++) {
        final mineCell = fieldRows[thisRow][thisColumn] as BlocMineCell;
        mineCell.resetPresentation.add(MineCellPresentation.unrevealed);
        if (shouldThisCellRandomlyIsBomb(
            remaininBomb, remainingCell, randomGen)) {
          mineCell.isBombSubject.add(true);
          remaininBomb--;
        } else {
          mineCell.isBombSubject.add(false);
        }
        remainingCell--;
      }
    }
  }
  */

  void _connectThisCellToItsNeighbor(thisRow, thisColumn, mineCell) {
    if (thisColumn > 0) {
      final itsMidLeft = fieldRows[thisRow][thisColumn - 1];
      itsMidLeft.neighbor.add(mineCell);
      mineCell.neighbor.add(itsMidLeft);
    }
    if (thisRow > 0) {
      if (thisColumn > 0) {
        final itsTopLeft = fieldRows[thisRow - 1][thisColumn - 1];
        itsTopLeft.neighbor.add(mineCell);
        mineCell.neighbor.add(itsTopLeft);
      }
      {
        final itsTopCenter = fieldRows[thisRow - 1][thisColumn];
        itsTopCenter.neighbor.add(mineCell);
        mineCell.neighbor.add(itsTopCenter);
      }
      if (thisColumn < dimension.x - 1) {
        final itsTopRight = fieldRows[thisRow - 1][thisColumn + 1];
        itsTopRight.neighbor.add(mineCell);
        mineCell.neighbor.add(itsTopRight);
      }
    }
  }

  dispose() {
    for (var i = 0; i < dimension.y; i++) {
      for (var j = 0; j < dimension.x; j++) {
        fieldRows[i][j].dispose();
      }
    }
  }

  @override
  IBlocMineCell cellAtLinearPosition(int position, bool isVerticalAxis) {
    int rowNumber;
    int columnNumber;
    if (isVerticalAxis) {
      rowNumber = (position / columnCount).floor();
      columnNumber = (position % columnCount);
    } else {
      columnNumber = (position / rowCount).floor();
      rowNumber = (position % rowCount);
    }

    return fieldRows[rowNumber][columnNumber];
  }

  @override
  int get cellCount => dimension.x * dimension.y;

  @override
  int get columnCount => dimension.x;

  @override
  int get rowCount => dimension.y;
}

class BlocMineField implements IBlocMineField {
  final BehaviorSubject<void> resetNewGame = BehaviorSubject<void>.seeded(null);
  final BehaviorSubject<GameState> gameState = BehaviorSubject();
  final BehaviorSubject<Point<int>> flagCell = BehaviorSubject<Point<int>>();
  final BehaviorSubject<Point<int>> openCell = BehaviorSubject<Point<int>>();
  final BehaviorSubject<int> elapsedTime = BehaviorSubject<int>();
  final BehaviorSubject<int> flagsLeft = BehaviorSubject<int>();

  final BehaviorSubject<MineField> field;

  static final columnCount = 10;
  static final rowCount = 8;
  static final bombCount = 10;

  BlocMineField([randomSeed])
      : field = BehaviorSubject<MineField>.seeded(MineField(
            Point(columnCount, rowCount), bombCount,
            randomSeed: randomSeed)) {
    resetNewGame.withLatestFrom<MineField, MineField>(field, (_, mineField) {
      mineField.dispose();
      return MineField(Point(columnCount, rowCount), bombCount);
      /*
      // @todo not yet FRP
      mineField.reset(10);
      return mineField;
      */
    }).pipe(field);

    final mergedBlocCellPresentation = field.flatMap((field) {
      final flattenedBlocCellPresentation = field.fieldRows
          .expand((list) => list)
          .map((blocCell) => blocCell.cellPresentation);
      return Observable.merge(flattenedBlocCellPresentation);
    });

    resetNewGame.map((_) => GameState.intro).listen((s) => gameState.add(s));
    mergedBlocCellPresentation
        .where(
            (thisPresnetation) => thisPresnetation == MineCellPresentation.bomb)
        .map((_) => GameState.gameover)
        .listen((s) => gameState.add(s));

    gameState.where((s) => s == GameState.intro).switchMap<int>((s) {
      return mergedBlocCellPresentation
              .where((thisPresnetation) =>
                  thisPresnetation == MineCellPresentation.flagged ||
                  thisPresnetation == MineCellPresentation.uncertain)
              .scan<int>((acc, current, i) {
        if (current == MineCellPresentation.flagged) {
          return acc - 1;
        }
        return acc + 1;
      }, bombCount).mergeWith([Observable.just(bombCount)])
          // @todo `bombCount` is not technically FRP
          ;
    }).pipe(flagsLeft);

    field
        .flatMap((f) => f.allCellsAreOpened)
        .map((_) => GameState.win)
        .listen((s) => gameState.add(s));

    field
        .flatMap((f) => f.firstCellsAreOpened)
        .map((_) => GameState.ongoing)
        .listen((s) => gameState.add(s));

    gameState.switchMap<int>((s) {
      switch (s) {
        case GameState.ongoing:
          return Observable.periodic(Duration(seconds: 1), (i) => i + 1);
        case GameState.intro:
          return Observable.just(0);
        default:
          return Observable.empty();
      }
    }).pipe(elapsedTime);

    flagCell
        .withLatestFrom<MineField, IBlocMineCell>(
            field,
            (pointToFlag, field) =>
                field.fieldRows[pointToFlag.y][pointToFlag.x])
        // @todo not yet FRP
        .listen((cell) => cell.interact.add(MineCellInteraction.nextFlag));

    openCell
        .withLatestFrom<MineField, IBlocMineCell>(
            field,
            (pointToFlag, field) =>
                field.fieldRows[pointToFlag.y][pointToFlag.x])
        // @todo not yet FRP
        .listen((cell) => cell.interact.add(MineCellInteraction.reveal));
  }

  @override
  void dispose() {
    resetNewGame.close();
    gameState.close();
    field.close();
    flagCell.close();
    elapsedTime.close();
    flagsLeft.close();
  }
}
