import 'dart:math';

import 'package:rx_minesweeper/interface_minecell.dart';

enum GameState {
  intro,
  ongoing,
  gameover,
  win,
}

abstract class IMineField {
  int get columnCount;
  int get rowCount;
  int get cellCount;
  IBlocMineCell cellAtLinearPosition(int position, bool isVerticalAxis);
}

abstract class IBlocMineField {
  Sink<void> get resetNewGame;
  Sink<Point<int>> get flagCell;
  Sink<Point<int>> get openCell;

  Stream<GameState> get gameState;
  Stream<IMineField> get field;
  Stream<int> get elapsedTime;
  Stream<int> get flagsLeft;

  void dispose();
}
