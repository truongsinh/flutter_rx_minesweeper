import 'package:flutter/material.dart';
import 'package:rx_minesweeper/widget_minecell.dart';
import 'interface_minefield.dart';

class GameActivity extends StatefulWidget {
  final IBlocMineField blocMineField;
  GameActivity(this.blocMineField);

  @override
  _GameActivityState createState() => _GameActivityState();
}

class _GameActivityState extends State<GameActivity> {
  @override
  void initState() {
    super.initState();

    widget.blocMineField.gameState
        .where((s) => s == GameState.gameover || s == GameState.win)
        .listen((s) {
      Navigator.of(context).push(PageRouteBuilder(
          opaque: false,
          pageBuilder: (BuildContext context, _, __) {
            return AlertDialog(
              title: Text(s == GameState.win ? "Win!" : "Game Over!"),
              content: Text(s == GameState.win
                  ? "You have open all cells without bombs!"
                  : "You stepped on a mine!"),
              actions: <Widget>[
                FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                    widget.blocMineField.resetNewGame.add(null);
                  },
                  child: Text("Play again"),
                ),
              ],
            );
          }));
    });
  }

  @override
  void dispose() {
    super.dispose();
    widget.blocMineField.dispose();
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        body: SafeArea(
            child: Column(
          children: <Widget>[
            Container(
              color: Colors.grey,
              height: 60.0,
              width: double.infinity,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  LCDDigitFromStream(widget.blocMineField.flagsLeft),
                  Spacer(),
                  InkWell(
                    onTap: () => widget.blocMineField.resetNewGame.add(null),
                    child: CircleAvatar(
                      child: Icon(
                        Icons.tag_faces,
                        color: Colors.black,
                        size: 40.0,
                      ),
                      backgroundColor: Colors.yellowAccent,
                    ),
                  ),
                  Spacer(),
                  LCDDigitFromStream(widget.blocMineField.elapsedTime),
                ],
              ),
            ),
            // The grid of squares
            StreamBuilder<IMineField>(
                stream: widget.blocMineField.field,
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return Container();
                  }
                  return Flexible(
                    child: LayoutBuilder(
                        builder: (context, BoxConstraints constraints) {
                      final screenRatio =
                          constraints.maxWidth / constraints.maxHeight;
                      final fieldRatio =
                          snapshot.data.columnCount / snapshot.data.rowCount;
                      final axis = screenRatio < fieldRatio
                          ? Axis.vertical
                          : Axis.horizontal;
                      final crossAxisCount = axis == Axis.vertical
                          ? snapshot.data.columnCount
                          : snapshot.data.rowCount;
                      return GridView.builder(
                        scrollDirection: axis,
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: crossAxisCount,
                        ),
                        itemBuilder: (context, position) {
                          final blocMineCell = snapshot.data
                              .cellAtLinearPosition(
                                  position, axis == Axis.vertical);
                          return MineCellWidget(blocMineCell);
                        },
                        itemCount: snapshot.data.cellCount,
                      );
                    }),
                  );
                }),
          ],
        )),
      );
}

class LCDDigitFromStream extends StatelessWidget {
  final Stream<int> stream;

  const LCDDigitFromStream(this.stream, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) => StreamBuilder(
        stream: stream,
        builder: (context, snapshot) {
          int elapsedTime = 0;
          if (snapshot.hasData) {
            elapsedTime = snapshot.data;
          }
          // @todo >1000 is okay but kind of overflow
          return Container(
            padding: EdgeInsets.fromLTRB(8, 8, 8, 0),
            child: FittedBox(
              fit: BoxFit.fitHeight,
              child: Text(
                elapsedTime.toString().padLeft(3, '0'),
                style: TextStyle(
                  fontSize: 999,
                  fontFamily: 'Monospace',
                  color: Colors.red,
                ),
              ),
            ),
          );
        },
      );
}
