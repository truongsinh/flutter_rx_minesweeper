import 'package:rxdart/subjects.dart';

import 'interface_minecell.dart';

class BlocMineCell implements IBlocMineCell {
  final neighborSubject = ReplaySubject<IBlocMineCell>();
  get neighbor => neighborSubject.sink;
  final interactSubject = BehaviorSubject<MineCellInteraction>();
  get interact => interactSubject;

  final isBombSubject = BehaviorSubject<bool>.seeded(false);
  get isBomb => isBombSubject.stream;
  final BehaviorSubject<MineCellPresentation> cellPresentationBahaviour;
  get cellPresentation => cellPresentationBahaviour.distinct();
  final resetPresentation = BehaviorSubject<MineCellPresentation>();

  BlocMineCell({initCellPresentation: MineCellPresentation.unrevealed})
      : cellPresentationBahaviour =
            BehaviorSubject<MineCellPresentation>.seeded(initCellPresentation) {
    // @todo short circut if this cell is a bomb
    final neighborBombStream = neighborSubject
        .flatMap((cell) => cell.isBomb)
        .map((isBomb) => isBomb ? 1 : 0)
        .scan<int>(
          (accumulated, currentValue, _) => accumulated += currentValue,
          0,
        )
        .distinct()
        .shareValueSeeded(0);

    interactSubject.stream
        .withLatestFrom3<int, MineCellPresentation, bool, MineCellPresentation>(
            neighborBombStream, cellPresentation, isBomb,
            (thisInteraction, latestNeighborBomb, latestPresentation, isBomb) {
          switch (thisInteraction) {
            case MineCellInteraction.nextFlag:
              switch (latestPresentation) {
                case MineCellPresentation.n0:
                case MineCellPresentation.n1:
                case MineCellPresentation.n2:
                case MineCellPresentation.n3:
                case MineCellPresentation.n4:
                case MineCellPresentation.n5:
                case MineCellPresentation.n6:
                case MineCellPresentation.n7:
                case MineCellPresentation.n8:
                case MineCellPresentation.bomb:
                  return latestPresentation;

                case MineCellPresentation.unrevealed:
                  return MineCellPresentation.flagged;

                case MineCellPresentation.flagged:
                  return MineCellPresentation.uncertain;

                case MineCellPresentation.uncertain:
                  return MineCellPresentation.unrevealed;
              }
              // todo should not reach this
              return latestPresentation;
            case MineCellInteraction.reveal:
              switch (latestPresentation) {
                case MineCellPresentation.n0:
                case MineCellPresentation.n1:
                case MineCellPresentation.n2:
                case MineCellPresentation.n3:
                case MineCellPresentation.n4:
                case MineCellPresentation.n5:
                case MineCellPresentation.n6:
                case MineCellPresentation.n7:
                case MineCellPresentation.n8:
                case MineCellPresentation.bomb:
                case MineCellPresentation.flagged:
                case MineCellPresentation.uncertain:
                  return latestPresentation;
                case MineCellPresentation.unrevealed:
                  if (isBomb) {
                    return MineCellPresentation.bomb;
                  }
                  return MineCellPresentation.values[latestNeighborBomb];
              }
          }
          // todo should not reach this
          return latestPresentation;
        })
        .distinct()
        .mergeWith([resetPresentation])
        .pipe(cellPresentationBahaviour);

    interactSubject.listen(null, onDone: resetPresentation.close);

    // @todo replace `listen` with other operator if possible
    cellPresentation
        .where((cellPres) => cellPres == MineCellPresentation.n0)
        .listen(
          (_) => neighborSubject.listen(
            (neighbor) => neighbor.interact.add(MineCellInteraction.reveal),
          ),
        );
  }

  @override
  void dispose() async {
    isBombSubject.close();
    neighborSubject.close();
    interactSubject.close();
    resetPresentation.close();
    // `cellPresentationBahaviour` is piped, thus should not be closed
  }
}
